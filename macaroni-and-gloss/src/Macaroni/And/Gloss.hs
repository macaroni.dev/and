{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}

module Macaroni.And.Gloss where

import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import qualified Sound.ALUT as ALUT

import Data.Unique
import qualified Data.Map.Strict as Map

--------------------------------------------------------------------

data Delta a = Delta
  { before :: a
  , after  :: a
  } deriving (Eq, Ord, Show, Read, Functor, Foldable, Traversable)

(-/-) :: a -> a -> Delta a
(-/-) = Delta

onAfter :: Delta a -> (a -> b) -> b
onAfter Delta{..} f = f after

(-&-) :: Delta a -> (a -> a) -> Delta a
Delta{..} -&- f = Delta{after = f after, ..}

(-%-) :: Delta a -> (a -> a) -> Delta a
Delta{..} -%- f = Delta{after = f after, before = after}

(-~-) :: Delta a -> a -> Delta a
Delta{..} -~- a = Delta{after = a, before = after}

(///) :: Delta a -> a -> Delta a
Delta{..} /// now = Delta { before = after, after = now }

--------------------------------------------------------------------

-- | TODO: Doc comment
data Scene layer = Scene { pictures :: Map.Map layer Gloss.Picture }
  deriving (Eq, Show)

instance Ord layer => Semigroup (Scene layer) where
  s1 <> s2 = Scene $ Map.unionWith (mappend @Gloss.Picture) (pictures s1) (pictures s2)

instance Ord layer => Monoid (Scene layer) where
  mempty = Scene Map.empty

mkScene :: Ord layer => layer -> Gloss.Picture -> Scene layer
mkScene l p = Scene $ Map.singleton l p

renderScene :: Ord layer => Scene layer -> Gloss.Picture
renderScene = mconcat . fmap snd . Map.toAscList . pictures

--------------------------------------------------------------------

data SoundCmd =
    SoftPlay Sound
  | HardPlay Sound
  | Stop Sound
  | Pause Sound

-- TODO: Looping? idk where

data Sound = Sound { soundUnique :: Unique, soundBuffer :: ALUT.Buffer, soundTag :: Maybe String }

loadWAV :: FilePath -> IO Sound
loadWAV path = do
  soundUnique <- newUnique
  soundBuffer <- ALUT.createBuffer (ALUT.File path)
  pure Sound { soundTag = Nothing, ..}

execSoundCmd :: SoundState -> SoundCmd -> IO SoundState
execSoundCmd = undefined

data SoundState = SoundState (Map.Map Unique (Map.Map (Maybe String) ALUT.Source))

--------------------------------------------------------------------

data PlaySoundWorld world = PlaySoundWorld
  { _pswWorlds :: Delta world
  , _pswSound :: SoundState
  }

playSoundIO
  :: Gloss.Display        -- ^ Display mode.
  -> Gloss.Color          -- ^ Background color.
  -> Int                  -- ^ Number of simulation steps to take for each second of real time.
  -> world                -- ^ The initial world.
  -> (world -> IO Gloss.Picture)   -- ^ A function to convert the world a picture.
  -> (Gloss.Event -> world -> IO world)
  -- ^ A function to handle input events.
  -> (Float -> world -> IO world)
  -- ^ A function to step the world one iteration.
  --   It is passed the period of time (in seconds) needing to be advanced.
  -> (Delta world -> IO SoundCmd)
  -> IO ()
playSoundIO display bgColor fps initWorld draw handleEvent tick doSound =
  ALUT.withProgNameAndArgs ALUT.runALUT $ \_ _ -> do
    Gloss.playIO
      display
      bgColor
      fps
      (PlaySoundWorld (initWorld -/- initWorld) (SoundState mempty))
      ioDraw
      ioHandle
      ioTick
  where
    ioDraw (PlaySoundWorld (Delta _ w) _) = draw w

    ioHandle e PlaySoundWorld{..} = do
      w <- _pswWorlds `onAfter` handleEvent e
      let dw' = _pswWorlds -~- w
      soundState' <- doSound dw'  >>= execSoundCmd _pswSound
      pure PlaySoundWorld{_pswWorlds = dw', _pswSound = soundState'}

    ioTick t PlaySoundWorld{..} = do
      w <- _pswWorlds `onAfter` tick t
      let dw' = _pswWorlds -~- w
      soundState' <- doSound dw' >>= execSoundCmd _pswSound
      pure PlaySoundWorld{_pswWorlds = dw', _pswSound = soundState'}

playSound
  :: Gloss.Display        -- ^ Display mode.
  -> Gloss.Color          -- ^ Background color.
  -> Int                  -- ^ Number of simulation steps to take for each second of real time.
  -> world                -- ^ The initial world.
  -> (world -> Gloss.Picture)   -- ^ A function to convert the world a picture.
  -> (Gloss.Event -> world -> world)
  -- ^ A function to handle input events.
  -> (Float -> world -> world)
  -- ^ A function to step the world one iteration.
  --   It is passed the period of time (in seconds) needing to be advanced.
  -> (Delta world -> SoundCmd)
  -> IO ()
playSound display bgColor fps initWorld draw handleEvent tick doSound =
  playSoundIO display bgColor fps initWorld drawIO handleEventIO tickIO doSoundIO
  where
    drawIO = pure . draw
    handleEventIO e w = pure $ handleEvent e w
    tickIO t w = pure $ tick t w
    doSoundIO = pure . doSound
