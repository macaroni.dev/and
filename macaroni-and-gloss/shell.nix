{ pkgs ? import <nixpkgs> {}, useNixHaskell ? false }:

with pkgs;
if useNixHaskell
then (haskellPackages.callCabal2nix "macaroni-and-gloss" ./. {}).env
else mkShell rec {
  buildInputs = [ freealut openal freeglut libGL libGLU ];
  
  # Ensure that libz.so and other libraries are available to TH
  # splices, cabal repl, etc.
  # See https://discourse.nixos.org/t/shared-libraries-error-with-cabal-repl-in-nix-shell/8921/8
  LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
}
